# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/nasser/Smart_Home/build/main.cpp_autogen/mocs_compilation.cpp" "/home/nasser/Smart_Home/build/CMakeFiles/main.cpp.dir/main.cpp_autogen/mocs_compilation.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  "QT_XML_LIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "../"
  "main.cpp_autogen/include"
  "/usr/include/qt4/QtGui"
  "/usr/include/qt4/QtCore"
  "/usr/share/qt4/mkspecs/default"
  "/usr/include/qt4"
  "/usr/include/qt4/QtXml"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
